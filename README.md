## Hosted URL
[https://dreamy-blackwell-af3448.netlify.app/](https://dreamy-blackwell-af3448.netlify.app/)

## How to Run

- cd flask_backend/api/app && virtualenv venv && source ./venv/bin/activate && cd ../../../
- cd react_frontend && npm i && cd ..
- cd flask_backend/api/app && flask run && cd ../../..
- cd react_frontend/ && yarn start
